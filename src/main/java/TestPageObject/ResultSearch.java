package TestPageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultSearch {

    private WebDriver driver;

    public ResultSearch(WebDriver driver) {
        this.driver = driver;
    }

    public String getFirstVideo() {

        return driver.findElement(By.xpath("//a[@class='yt-simple-endpoint style-scope ytd-video-renderer']")).getText();
    }
}
