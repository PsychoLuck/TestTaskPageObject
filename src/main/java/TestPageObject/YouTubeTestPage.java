package TestPageObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class YouTubeTestPage {
    private ChromeDriver driver;

    @Before
    public void startDriver(){
        System.setProperty("webdriver.chrome.driver", "../TestPageObject/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.youtube.com/");
    }

    @After
    public void closeDriver(){
        driver.close();
    }

    @Test
    public void testSearch1() throws InterruptedException {
        HomePageYouTube home = new HomePageYouTube(driver);
        ResultSearch resultserch = home.search("Page Object");
        assertTrue(resultserch.getFirstVideo().contains("PageObject"));//проверка, что название первой сылки содержит поисковой запрос
    }

    @Test //тест который не должен пройти проверку и отдаст AssertionError
    public void testSearch2() throws  InterruptedException{
        HomePageYouTube home = new HomePageYouTube(driver);
        ResultSearch resultSearch = home.search("Java Selenium WebDriver");
        assertTrue(resultSearch.getFirstVideo().contains("PageObject"));
    }

}
