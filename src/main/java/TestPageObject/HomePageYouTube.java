package TestPageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class HomePageYouTube {

    private WebDriver driver;

    public HomePageYouTube(ChromeDriver driver) {
        this.driver = driver;
    }

    public ResultSearch search(String string) throws InterruptedException {
        driver.findElement(By.xpath("//input[contains(@id, 'search')]")).sendKeys(string);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(1);//по хорошему написать было так driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//button[contains(@id, 'search-icon-legacy')]")).click();
        TimeUnit.SECONDS.sleep(2);//или так driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);


        return new ResultSearch(driver);
    }
}
